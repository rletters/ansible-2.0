rletters-ansible
================

**Author:** Charles Pence  
**Contributors:** See Contributors [at RLetters](https://codeberg.org/rletters/rletters)  
**Copyright:** 2013--2018 Charles Pence  
**License:** MIT License  

**NOTE: This set of playbooks is intended for deploying RLetters 2.0, not version 3.0 as released in May, 2018 (and later). It is no longer under development, as deployment can now be performed with Heroku.**

This repository contains a collection of Ansible playbooks for deploying [RLetters.](https://codeberg.org/rletters/rletters/)

Copyright
---------

RLetters &copy; 2011–2018 [Charles Pence](mailto:charles@charlespence.net). RLetters is licensed under the MIT license. Please see the {file:COPYING} document for more information.
